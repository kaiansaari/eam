const config = {
    CHAIN_APIURI:  process.env.CHAIN_APIURI,
    CHAIN_NET: process.env.CHAIN_NET,
    TEST_ADDRESS: process.env.TEST_ADDRESS,
    BNB: process.env.BNB,
    CAN: process.env.CAN,
}

exports.app = config
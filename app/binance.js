const BnbApiClient = require('@binance-chain/javascript-sdk')
const app = require('../config/config').app

var bnbClient = {}

// Start Binance Chain Client //
const binanceConnect = async() => {
    try{
        bnbClient = new BnbApiClient(app.CHAIN_APIURI)
        bnbClient.chooseNetwork(app.CHAIN_NET)
        await bnbClient.initChain()
        return bnbClient;
    } catch(error){
        console.log(error)
    }
}

// Get Balance //
const getBalance = (address) => {
    return bnbClient.getBalance (address)
}

// Exports //
exports.binanceConnect = binanceConnect
exports.getBalance = getBalance


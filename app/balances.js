
const binance = require('./binance')
const app = require('../config/config').app

app.toSell = []

const startBinanceClient = async() => {
    // Start Binance Client //
    try {
        await binance.binanceConnect()
        console.log('Binance Chain Connect: OK')
    } catch (error) {}
}

const getEscrowAssets = async() => { 
    // Get Escrow Balances //
    try { 
        escrowBalance = await binance.getBalance(app.TEST_ADDRESS)
    } catch (error) {
        throw new Error('BINANCE_ERROR')
    }
    
    let newBalances = [];

    // Identify sellable non-CAN/BNB assets
    for (assets of escrowBalance) {
        if (assets.symbol == app.CAN || assets.symbol == app.BNB) {
            continue;
        } else {
            newBalances.push({symbol: assets.symbol, amount: assets.free})
        }
    }

    return newBalances;
}

const assetCheck = async() => {
    let assetsToSell = await getEscrowAssets();
    
    if(assetsToSell && assetsToSell.length) {
        console.log('Sellable Assets:')
        console.log(assetsToSell)
    } else {
        return false;
    }

}

const main = async() => {
    await startBinanceClient();
    await assetCheck();
}

main();

exports.startBinanceClient = startBinanceClient
exports.getEscrowAssets = getEscrowAssets
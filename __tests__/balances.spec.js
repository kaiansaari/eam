
require('dotenv').config()
const app = require('../config/config').app
const balances = require('../app/balances')
const binance = require('../app/binance')
const fs = require('fs')


const testdata = JSON.parse(fs.readFileSync('./__mockData__/balances.json'))

describe('SMOKE TESTS', () => {
  beforeAll(async() => {
    // something
  });

  beforeEach(async() => {
   //something
  })

  test('Balances are returned', async() => {
    const mockBalances = jest.spyOn(binance, "getBalance");
    mockBalances.mockImplementation(async() =>{return testdata.happy});

    let assetsToSell = [
      { symbol: 'XRP.B-585', amount: '1.0000000' },
      { symbol: 'RUNE-A1F', amount: '100.00000000' }
      ]

    let result = await balances.getEscrowAssets();
    expect(result).toEqual(assetsToSell)
  });
  
  test('No balances are returned', async() => {

    const mockBalances = jest.spyOn(binance, "getBalance");
    mockBalances.mockImplementation(async() =>{return testdata.zero});

    let assetsToSell = []

    let result = await balances.getEscrowAssets();
    expect(result).toEqual(assetsToSell)
  });

  test('No balances are returned if less than 1', async() => {

    const mockBalances = jest.spyOn(binance, "getBalance");
    mockBalances.mockImplementation(async() =>{return testdata.lessThanOne});

    let assetsToSell = [
      { symbol: 'XRP.B-585', amount: '1.0000000' }
    ]

    let result = await balances.getEscrowAssets();
    expect(result).toEqual(assetsToSell)
  });

});
